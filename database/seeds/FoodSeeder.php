<?php

use App\Models\Food;
use App\Models\Ingredient;
use Illuminate\Database\Seeder;


class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foodsJson = File::get(public_path('foods.json'));
        $foods = json_decode($foodsJson, true);
        $foods = collect($foods['recipes']);
        foreach ($foods as $food) {
            $storedFood = Food::create(['title' => $food['title']]);
            $ingredientIds = Ingredient::whereIn('title', $food['ingredients'])->pluck('id');
            $storedFood->ingredients()->attach($ingredientIds);
        }
    }
}
