<?php

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredientJson = File::get(public_path('ingredients.json'));
        $ingredients = json_decode($ingredientJson, true);
        $ingredients = collect($ingredients['ingredients']);
        foreach ($ingredients as $ingredient) {
            Ingredient::create($ingredient);
        }
    }
}
