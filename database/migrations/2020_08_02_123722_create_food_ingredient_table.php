<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodIngredientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_ingredient', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('food_id')->unsigned()->index();
            $table->foreign('food_id')
                ->references('id')
                ->on('foods');

            $table->bigInteger('ingredient_id')->unsigned()->index();
            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foot_ingredient');
    }
}
