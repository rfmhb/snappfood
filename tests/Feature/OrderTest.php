<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("db:seed");
    }

    public function testOrderRegister()
    {
        $response = $this->post('/api/order', ['food_id' => 1]);
        $response->assertStatus(200);

        $response = $this->post('/api/order', ['food_id' => 1]);
        $response->assertStatus(200);

        $response = $this->post('/api/order', ['food_id' => 1]);
        $response->assertStatus(200);

        $response = $this->post('/api/order', ['food_id' => 1]);
        $response->assertStatus(404);

    }
}
