<?php

use App\Http\Controllers\FoodController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

Route::get('menu', [FoodController::class, 'menu']);
Route::post('order', [FoodController::class, 'order']);

Route::get('test', [TestController::class, 'test']);
