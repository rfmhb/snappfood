## About Laravel

this is a sample project for snappfood hiring:

- Repository pattern
- Dockerize
- Dependency Injection 
- Test
- etc ...



## how to use
install docker and docker-compose then run these commands.

    composer install
    docker-compose up -d
    docker exec -it snapp php artisan migrate
    docker exec -it snapp php artisan db:seed
This link is an example of that: [http://localhost:81](http://localhost:81)
