FROM hitalos/php:latest
LABEL maintainer="mhbolivand"
COPY . /app
WORKDIR /app
CMD php ./artisan serve --port=80 --host=0.0.0.0
HEALTHCHECK --interval=1m CMD curl -f http://localhost/api/test || exit 1
EXPOSE 80
