<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Repository\Contract\FoodRepoInterface;
use App\Repository\Contract\IngredientRepoInterface;
use Illuminate\Http\Response;

class FoodController extends Controller
{

    public function menu(FoodRepoInterface $foodRepo)
    {
        $foods = $foodRepo->foodList();
        return response()->json($foods);
    }

    public function order(OrderRequest $request,
                          FoodRepoInterface $foodRepo,
                          IngredientRepoInterface $ingredientRepo)
    {
        $food_id = $request->get('food_id');
        $exists = $foodRepo->isAvailable($food_id);
        if (!$exists) {
            return response()->json(['error' => "this food doesn't exists."], Response::HTTP_NOT_FOUND);
        }
        $ingredients = $foodRepo->find($food_id)->ingredients();
        $ingredientRepo->decreaseIngredients($ingredients);
        return response()->json(['Your order has been registered']);
    }

}
