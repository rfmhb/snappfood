<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = [
        'title', 'best-before', 'expires-at', 'stock'
    ];

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }
}
