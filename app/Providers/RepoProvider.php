<?php

namespace App\Providers;

use App\Repository\Contract\FoodRepoInterface;
use App\Repository\Contract\IngredientRepoInterface;
use App\Repository\Contract\RepoInterface;
use App\Repository\FoodRepo;
use App\Repository\IngredientRepo;
use Illuminate\Support\ServiceProvider;

class RepoProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FoodRepoInterface::class, FoodRepo::class);
        $this->app->bind(IngredientRepoInterface::class, IngredientRepo::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
