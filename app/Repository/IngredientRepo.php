<?php


namespace App\Repository;


use App\Models\Ingredient;
use App\Repository\Contract\IngredientRepoInterface;

class IngredientRepo implements IngredientRepoInterface
{

    public function decreaseIngredient($id): void
    {
        $ingredient = Ingredient::find($id);
        $ingredient->update('stock', $ingredient->stock - 1);
    }

    public function decreaseIngredients(array $ids): void
    {
        $ingredients = Ingredient::whereIn('id', $ids)->get();
        foreach ($ingredients as $ingredient) {
            $ingredient->update(['stock' => $ingredient->stock - 1]);
        }
    }
}
