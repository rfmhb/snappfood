<?php


namespace App\Repository\Contract;


use Illuminate\Support\Collection;

interface FoodRepoInterface
{
    public function isAvailable($id): bool;

    public function ingredients(): array;

    public function find($id): FoodRepoInterface;

    public function foodList(): Collection;

    public function expiresAndStockFilter(): FoodRepoInterface;

    public function bestIngredient($best = true): FoodRepoInterface;

    public function get($columns = ['*']): Collection;
}
