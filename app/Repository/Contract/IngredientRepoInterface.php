<?php


namespace App\Repository\Contract;


interface IngredientRepoInterface
{
    public function decreaseIngredient($id): void;

    public function decreaseIngredients(array $id): void;
}
