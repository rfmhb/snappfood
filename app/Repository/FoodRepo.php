<?php


namespace App\Repository;


use App\Models\Food;
use App\Repository\Contract\FoodRepoInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class FoodRepo implements FoodRepoInterface
{
    private Builder $query;
    private Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection();
        $this->query = Food::query();
    }

    public function foodList(): Collection
    {
        $this->expiresAndStockFilter()
            ->bestIngredient()
            ->bestIngredient(false);
        return $this->collection;
    }

    public function ingredients(): array
    {
        return $this->query->with('ingredients')->pluck('id')->toArray();
        //return $this;
    }


    public function isAvailable($id): bool
    {
        return $this->find($id)
            ->expiresAndStockFilter()
            ->get()->count();
    }

    public function find($id): FoodRepoInterface
    {
        $this->query->find($id);
        return $this;
    }

    public function expiresAndStockFilter(): FoodRepoInterface
    {
        $this->query->whereDoesntHave('ingredients',
            function ($ingredients) {
                //todo: change hard code with now()
                return $ingredients->where('expires-at', '<', "2020-07-13")
                    ->orWhere('stock', '<=', 0);
            });
        return $this;
    }

    public function bestIngredient($best = true): FoodRepoInterface
    {
        $condition = $best ? '<' : '>=';
        $collection = $this->query->whereDoesntHave('ingredients',
            function ($ingredients) use ($condition) {
                return $ingredients->where('best-before', $condition, "2020-07-13");
            })->get();
        $this->collection->push($collection);
        $this->collection = $this->collection->flatten();
        return $this;
    }

    public function get($columns = ['*']): Collection
    {
        return $this->query->get($columns);
    }

}
